// See the Electron documentation for details on how to use preload scripts:
// https://www.electronjs.org/docs/latest/tutorial/process-model#preload-scripts
const { contextBridge, ipcRenderer } = require("electron");

/*
	Events definieren, die eine Kommunikation zwischen Main Process und
	Renderer Process erlauben.

	Der key "electron" ist beliebig. Im Renderer können wir nun alle hier definierten Funktionen
	und window.electron aufrufen
*/
contextBridge.exposeInMainWorld("electron", {
	/*
		Im Renderer kann nun das Event onTestEvent getriggert werden. Ist das der Fall,
		fängt das die contextBridge ab und sendet das Event testEvent in diesem Fall
		an den main process.
	*/
	getVideoSources: async () => {
		ipcRenderer.send("get-video-sources")
	},
	// Wir können von main mit dem Renderer kommunizieren!
	on: (eventName, func) => {
		ipcRenderer.on(eventName, (event, ...args) => func(...args));
	}



	// onTestEvent: (text, number) => {
	// 	/*
	// 		Wir senden ein Event aus, das jetzt im main Context abgefangen
	// 		werden kann.
	// 	*/
	// 	// Es können nach dem Event Namen beliebige Attribute (args) mitgegeben werden
	// 	ipcRenderer.send('testEvent', text, number);
	// }

});
