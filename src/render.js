// Zugriff auf ipc Process um Events auszulösen. electron ist der api key aus preload.js
const btnSelect = document.getElementById('btnSelect');
btnSelect.onclick = (e) => {
	window.electron.getVideoSources();
};

const videoPreview = document.getElementById('videoPreview');

// Events aus main process abfangen
window.electron.on("selectSource", async (source) => {
	btnSelect.innerText = source.name;

	// Video Stream konfigurieren
	const constraints = {
		audio: false,
		video: {
			mandatory: {
				chromeMediaSource: "desktop",
				chromeMediaSourceId: source.id
			}
		}
	};

	const stream = await navigator.mediaDevices.getUserMedia(constraints);
	// Quelle des Videos
	videoPreview.srcObject = stream;
	videoPreview.play();
});



// Zum Testen der Kommunikation mit dem main process.
// const fieldText = document.getElementById('fieldText');
// const fieldNumber = document.getElementById('fieldNumber');
// const btnSend = document.getElementById('btnSend')

// btnSend.onclick = (e) => {
// 	window.electron.onTestEvent(fieldText.value, fieldNumber.value);
// }

