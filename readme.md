# Screen recorder

## Inhaltsverzeichnis
- [Screen recorder](#screen-recorder)
	- [Inhaltsverzeichnis](#inhaltsverzeichnis)
	- [Ordner mit electron forge erstellen](#ordner-mit-electron-forge-erstellen)
		- [Eigene Dateien](#eigene-dateien)
	- [Hot reloading nicht möglich](#hot-reloading-nicht-möglich)


Based on: https://www.youtube.com/watch?v=3yqDxhR2XxE&t=1s

## Ordner mit electron forge erstellen

```npx create-electron-app name-of-app```

Es wird eine funktionierende Basis Applikation mit allen notwendigen Dateien erstellt.

Im Verzeichnis ```src```finden wir

- ```index.html``` - das wird angezeigt
- ```index.js``` - main process von electron
- ```preload.js``` -  wird ausgeführt bevor Webinhalte geladen werden. Ermöglicht Zugriff auf nodejs APIs.

Alternativ zu ```preload.js``` können wir in den webPreferences des BrowserWindow Objekts nodeIntegration: true setzen.

### Eigene Dateien

Wir erstellen die Datei ```render.js```, die vom Render Prozess verwendet wird

## Hot reloading nicht möglich

Wir können entweder Cmd/Strg + Shift + R (Reload) im App Fenster drücken, oder in der Konsole ```rs```
(restart) eingeben.

